﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Acme.Services.Client;
using Acme.Services.Dto;
using Acme.Web.ViewModels;

namespace Acme.Web.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly IWebServiceClient<EmployeeDto> _client;

        public EmployeeController() : this( new WebServiceClient<EmployeeDto>()) {}

        public EmployeeController( IWebServiceClient<EmployeeDto> client )
        {
            _client = client;
        }

        [HttpGet]
        public async Task<ActionResult> Index()
        {
            var employees = await _client.GetAllAsync();

            return View( employees );
        }

        [HttpGet]
        public ActionResult Create()
        {            
           return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create( [Bind( Include = "FirstName,LastName,BirthDate,EmployeeNum,EmployedDate,TerminatedDate" )] PersonEmployeeVm model )
        {
            if ( ModelState.IsValid )
            {
                EmployeeDto dto = new EmployeeDto();
                dto.Person.FirstName = model.FirstName;
                dto.Person.LastName = model.LastName;
                dto.Person.BirthDate = model.BirthDate;
                dto.EmployeeNum = model.EmployeeNum;
                dto.EmployedDate = model.EmployedDate;
                dto.TerminatedDate = model.TerminatedDate;

                OperationResult op = await _client.AddAsync( dto );

                if ( !op.Success ) 
                {
                    ModelState.AddModelError( "", "Problem creating" );
                    return View( model );
                }

                return RedirectToAction( "Index" );
            }

            return View( model  );
        }

    }
}