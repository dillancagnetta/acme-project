﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Acme.Web.ViewModels
{
    public class PersonEmployeeVm
    {
        [Display( Name = "First Name" )]
        [StringLength( 128 )]
        public string FirstName { get; set; }

        [Display( Name = "Last Name" )]
        [StringLength( 128 )]
        public string LastName { get; set; }

        [Display( Name = "Birth Date" )]
        [DataType( DataType.Date, ErrorMessage = "Please enter a valid date in the format dd/mm/yyyy" )]
        public DateTime BirthDate { get; set; }

        [Display( Name = "Employee No." )]
        [StringLength( 16 )]
        public string EmployeeNum { get; set; }

        [Display( Name = "Employed Date" )]
        [DataType( DataType.Date, ErrorMessage = "Please enter a valid date in the format dd/mm/yyyy" )]
        public DateTime EmployedDate { get; set; }

        [Display( Name = "Terminated Date" )]
        [DataType( DataType.Date, ErrorMessage = "Please enter a valid date in the format dd/mm/yyyy" )]
        public DateTime? TerminatedDate { get; set; }
    }
}