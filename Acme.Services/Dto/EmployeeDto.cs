﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acme.Services.Dto
{
    public class EmployeeDto
    {
        public int Id { get; set; }        
        public string EmployeeNum { get; set; }
        public DateTime EmployedDate { get; set; }
        public DateTime? TerminatedDate { get; set; }
        public PersonDto Person { get; set; }

        public EmployeeDto()
        {
            Person = new PersonDto();
        }
    }

}
