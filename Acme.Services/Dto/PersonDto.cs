﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acme.Services.Dto
{
    public class PersonDto
    {
        public string FirstName { get; set; }        
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
    }
}
