﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using Acme.Data;
using Acme.Domain.Model;
using Acme.Services.Dto;
using Acme.Services.Interfaces;

namespace Acme.Services.Model
{
    public partial class EmployeeService : Service<Employee>, IEmployeeService
    {
        public EmployeeService( AcmeContext context ) : base( context )
        {
        }

        /// <summary>
        /// Determines whether this instance can delete the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="errorMessage">The error message.</param>
        /// <returns>
        ///   <c>true</c> if this instance can delete the specified item; otherwise, <c>false</c>.
        /// </returns>
        public bool CanDelete( Employee item, out string errorMessage )
        {
            errorMessage = string.Empty;

            return true;
        }

        /// <summary>
        /// Gets the employees.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<EmployeeDto> GetEmployees()
        {
            return Queryable( "Person" )
              .Select( e => new EmployeeDto
              {
                  Id = e.Id,
                  EmployeeNum = e.EmployeeNum,
                  EmployedDate = e.EmployedDate,
                  TerminatedDate = e.TerminatedDate,
                  Person = new PersonDto
                  {
                      FirstName = e.Person.FirstName,
                      LastName = e.Person.LastName,
                      BirthDate = e.Person.BirthDate
                  }
              } )
              .ToList();
        }

        public EmployeeDto GetEmployee( int id )
        {
            return Queryable( "Person" )
                .Select( e => new EmployeeDto
                {
                    Id = e.Id,
                    EmployeeNum = e.EmployeeNum,
                    EmployedDate = e.EmployedDate,
                    TerminatedDate = e.TerminatedDate,
                    Person = new PersonDto
                    {
                        FirstName = e.Person.FirstName,
                        LastName = e.Person.LastName,
                        BirthDate = e.Person.BirthDate
                    }
                } )
                .FirstOrDefault( e => e.Id == id );
        }


        public Employee FromDto( EmployeeDto dto)
        {
            Employee employee = new Employee();
            employee.Person = new Person();

            employee.EmployedDate = dto.EmployedDate;
            employee.EmployeeNum = dto.EmployeeNum;
            employee.TerminatedDate = dto.TerminatedDate;
            employee.Person.FirstName = dto.Person.FirstName;
            employee.Person.LastName = dto.Person.LastName;
            employee.Person.BirthDate = dto.Person.BirthDate;

            return employee;
        }

    }
}
