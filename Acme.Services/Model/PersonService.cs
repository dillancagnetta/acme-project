﻿using System.Linq;
using System.Runtime.Remoting.Contexts;
using Acme.Data;
using Acme.Domain.Model;

namespace Acme.Services.Model
{
    public partial class PersonService : Service<Person>
    {
        public PersonService( AcmeContext context ) : base( context )
        {
        }

        /// <summary>
        /// Determines whether this instance can delete the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="errorMessage">The error message.</param>
        /// <returns>
        ///   <c>true</c> if this instance can delete the specified item; otherwise, <c>false</c>.
        /// </returns>
        public bool CanDelete( Person item, out string errorMessage )
        {
            errorMessage = string.Empty;
            
            return true;
        }
       
    }
}
