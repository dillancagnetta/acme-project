﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Acme.Domain;
using Acme.Domain.Model;
using Acme.Services.Dto;

namespace Acme.Services.Interfaces
{
    public interface IEmployeeService : IService<Employee>
    {
        IEnumerable<EmployeeDto> GetEmployees();
        EmployeeDto GetEmployee( int id );
        Employee FromDto( EmployeeDto dto );
    }
}
