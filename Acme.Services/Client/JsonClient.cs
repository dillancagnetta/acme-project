﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Acme.Core.ExtensionMethods;
using Newtonsoft.Json;
using RockMobile.Services;
using RockMobile.Utilities;

namespace Acme.Services.Client
{
    public class JsonClient : BaseClient, IRestClient
    {
        protected override string GetFormat()
        {
            return "application/json";
        }

        public JsonClient()
        {
            HttpClient.DefaultRequestHeaders.Accept.Clear();
            HttpClient.DefaultRequestHeaders.Accept.Add( new MediaTypeWithQualityHeaderValue( GetFormat() ) );
        }

        /// <summary>
        /// Async POST method.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="address">The address.</param>
        /// <param name="dto">The dto.</param>
        /// <returns></returns>
        public async Task<ServiceResponse<T>> PostAsync<T>( string address, object dto )
        {
            //// post asynchronously
            try
            {
                //// serialize DTO to string
                string json = JsonConvert.SerializeObject( dto );

                StringContent content = new StringContent( json, Encoding.UTF8, GetFormat() );
                
                var response = await HttpClient.PostAsync( address, content );

                return await GetResponse<T>( response );

            }
            catch ( Exception ex )
            {
                return new ServiceResponse<T>( HttpStatusCode.InternalServerError, ex );
            }
        }

        /// <summary>
        ///Async POST method.
        /// </summary>
        /// <param name="address">The address.</param>
        /// <param name="dto">The DTO.</param>
        /// <returns>
        /// the Id of the created resource
        /// </returns>
        public async Task<ServiceResponse<int>> PostAsync( string address, object dto )
        {
            //// post asynchronously
            try
            {
                //// serialize DTO to string
                string json = JsonConvert.SerializeObject( dto );

                StringContent content = new StringContent( json, Encoding.UTF8, GetFormat() );
                
                var response = await HttpClient.PostAsync( address, content );

                return await GetResponse<int>( response );

            }
            catch ( Exception ex )
            {
                return new ServiceResponse<int>( HttpStatusCode.InternalServerError, ex );
            }
        }

        /// <summary>
        /// Async PATCH method.
        ///  PATCH api/<controller>/5  (update subset of atttributes)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="address">The address.</param>
        /// <param name="dto">The dto.</param>
        /// <returns></returns>
        /// <returns></returns>
        public async Task<ServiceResponse<string>> PatchAsync( string address, object dto )
        {
            //// update subset of attributes
            try
            {
                //// serialize DTO to string
                string json = JsonConvert.SerializeObject( dto );

                StringContent content = new StringContent( json, Encoding.UTF8, GetFormat() );
                
                var response = await HttpClient.PatchAsync( address, content );

                return new ServiceResponse<string>( response.StatusCode );
            }
            catch ( Exception ex )
            {
                return new ServiceResponse<string>( HttpStatusCode.InternalServerError, ex );
            }
        }


        public async Task<ServiceResponse<string>> PutAsync( string address, object dto )
        {
            //// update subset of attributes
            try
            {
                //// serialize DTO to string
                string json = JsonConvert.SerializeObject( dto );

                StringContent content = new StringContent( json, Encoding.UTF8, GetFormat() );

                var response = await HttpClient.PutAsync( address, content );

                return new ServiceResponse<string>( response.StatusCode );
            }
            catch ( Exception ex )
            {
                return new ServiceResponse<string>( HttpStatusCode.InternalServerError, ex );
            }
        }

        /// <summary>
        /// Async GET method
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="address">The address.</param>
        /// <returns></returns>
        public async Task<ServiceResponse<T>> GetAsync<T>( string address )
        {
            try
            {
                var response = await HttpClient.GetAsync( address );

                return await GetResponse<T>( response );
            }
            catch ( Exception ex )
            {
                return new ServiceResponse<T>( HttpStatusCode.InternalServerError, ex );
            }
        }

        /// <summary>
        /// Async GET method with query values
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="address">The address.</param>
        /// <param name="values">The values.</param>
        /// <returns></returns>
        public async Task<ServiceResponse<T>> GetAsync<T>( string address, Dictionary<string, string> values )
        {
            try
            {
                var builder = new StringBuilder( address );
                builder.Append( "?" );

                int length = values.Count;
                int index = 1;
                foreach ( var pair in values )
                {
                    if ( index != length )
                    {                        
                        builder.Append( $"{pair.Key}={pair.Value}&" );
                        index++;
                    }
                    else
                    {
                        builder.Append( $"{pair.Key}={pair.Value}" );
                    }
                }

                var response = await HttpClient.GetAsync( builder.ToString() );
                return await GetResponse<T>( response );
            }
            catch ( Exception ex )
            {
                return new ServiceResponse<T>( HttpStatusCode.InternalServerError, ex );
            }
        }

        
        #region Private Methods

        /// <summary>
        /// Gets the response.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="response">The response.</param>
        /// <returns></returns>
        private static async Task<ServiceResponse<T>> GetResponse<T>( HttpResponseMessage response )
        {
            var serviceResponse = new ServiceResponse<T>( response.StatusCode );

            if ( !response.IsSuccessStatusCode )
            {
                return serviceResponse;
            }

            try
            {
                //// get response string
                serviceResponse.Content = await response.Content.ReadAsStringAsync();
                //// serialize the response to object
                serviceResponse.Value = JsonConvert.DeserializeObject<T>( serviceResponse.Content );
            }
            catch ( Exception ex )
            {
                serviceResponse.Error = ex;
            }

            return serviceResponse;
        }        

        #endregion


    }
}
