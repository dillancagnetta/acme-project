﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Http;

/*Install-Package
 * 
 * Microsoft.AspNet.WebApi.Client.
 */

namespace Acme.Services.Client
{
    public abstract class BaseClient
    {
        protected static readonly HttpClient HttpClient = GetHttpClient();       
        
        /// <summary>
        /// Returns the implementation format e.g. "application/json"
        /// </summary>
        /// <returns></returns>
        protected abstract string GetFormat();

        #region Methods
        /// <summary>
        /// Gets the HTTP client.
        /// </summary>       
        private static HttpClient GetHttpClient()
        {
            string baseUri = ConfigurationManager.AppSettings["BaseApiUrl"];

            var httpClient = new HttpClient()
            {
                BaseAddress = new Uri( baseUri ),                
            };


            return httpClient;
        }

        #endregion


    }
}