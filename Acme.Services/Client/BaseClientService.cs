﻿using RockMobile.Services;

namespace Acme.Services.Client
{
    public class BaseClientService
    {
        protected readonly IRestClient HttpClient;

        protected BaseClientService( IRestClient httpClient )
        {
            HttpClient = httpClient;
        }
    }
}
