﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acme.Services.Client
{
    public interface IWebServiceClient<T>
    {
        /// <summary>
        /// Gets the employee asynchronously by id
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Task<T> GetAsync( int id );

        /// <summary>
        /// Gets all the employees asynchronously.
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<T>> GetAllAsync();

        /// <summary>
        /// Adds the asynchronous.
        /// </summary>
        /// <param name="dto">The dto.</param>
        /// <returns></returns>
        Task<OperationResult> AddAsync( object dto );
    }

}
