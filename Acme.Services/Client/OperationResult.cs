﻿using System.Collections.Generic;

namespace Acme.Services.Client
{
    public class OperationResult
    {
        public bool Success { get; set; }        

        public List<string> MessageList { get; private set; }

        public OperationResult()
        {
            MessageList = new List<string>();
            Success = false;
        }

        public void AddMessage( string message )
        {
            MessageList.Add( message );
        }

        public override string ToString()
        {
            return $"Success: {Success}";
        }
    }
}
