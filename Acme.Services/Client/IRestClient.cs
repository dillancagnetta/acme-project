﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RockMobile.Utilities;

namespace RockMobile.Services
{
    public interface IRestClient
    {
        /// <summary>
        /// Async POST method.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="address">The address.</param>
        /// <param name="dto">The DTO.</param>
        /// <param name="serializeResponse"></param>
        /// <returns></returns>
        Task<ServiceResponse<T>> PostAsync<T>( string address, object dto );

        /// <summary>
        /// Posts the asynchronous.
        /// </summary>
        /// <param name="address">The address.</param>
        /// <param name="dto">The dto.</param>
        /// <returns>the Id of the created resource</returns>
        Task<ServiceResponse<int>> PostAsync( string address, object dto );

        /// <summary>
        /// Async PATCH method. 
        /// // PATCH api/<controller>/5  (update subset of attributes)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="address">The address.</param>
        /// <param name="dto">The DTO.</param>
        /// <returns></returns>
        Task<ServiceResponse<string>> PatchAsync( string address, object dto );

        Task<ServiceResponse<string>> PutAsync( string address, object dto );

        /// <summary>
        /// Async GET method
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="address">The address.</param>
        /// <returns></returns>
        Task<ServiceResponse<T>> GetAsync<T>( string address );

        /// <summary>
        /// Async GET method with query values
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="address">The address.</param>
        /// <param name="values">The values.</param>
        /// <returns></returns>
        Task<ServiceResponse<T>> GetAsync<T>( string address, Dictionary<string, string> values );
    }
}