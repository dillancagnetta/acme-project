﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Acme.Domain;
using Acme.Domain.Model;
using Acme.Services.Dto;
using RockMobile.Services;

namespace Acme.Services.Client
{
    public class WebServiceClient<T> : BaseClientService, IWebServiceClient<T> where T :  new()
    {
        public WebServiceClient() : base( new JsonClient() )
        {
        }
        
        /// <summary>
        /// Gets the employee asynchronously by id
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public async Task<T> GetAsync( int id )
        {
            // Perform Rest Get Call
            var serviceResponse = await HttpClient.GetAsync<T>( ConfigurationManager.AppSettings["EmployeesEndPoint"] + $"/{id}" );
            // Error check
            if ( serviceResponse.StatusCode != HttpStatusCode.OK ) return default( T );
            // Get De-serialized object
            var model = serviceResponse.Value;

            return model;
        }

        /// <summary>
        /// Gets all the employees asynchronously.
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            // Perform Rest Get Call
            var serviceResponse = await HttpClient.GetAsync<IEnumerable<T>>( ConfigurationManager.AppSettings["EmployeesEndPoint"] );
            // Error check
            if ( serviceResponse.StatusCode != HttpStatusCode.OK ) return null;
            // Just did this to make it easier to read and understand
            var models = serviceResponse.Value;

            return models;
        }


        public async Task<OperationResult> AddAsync( object dto )
        {
            var op = new OperationResult();
            
            // Post
            var serviceResponse = await HttpClient.PostAsync<T>( ConfigurationManager.AppSettings["EmployeesEndPoint"], dto );
            // Check that there were no problems
            if ( serviceResponse.Error != null || serviceResponse.StatusCode != HttpStatusCode.Created ) return op;

            op.Success = true;            
            return op;
        }

    }
}
