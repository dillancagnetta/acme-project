﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using Acme.Core.ExtensionMethods;
using Acme.Domain;

namespace Acme.Services
{
    /// <summary>
    /// Generic POCO service class
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Service<T> : IService<T>  where T : Entity<T>, new()
    {

        #region Fields

        private DbContext _context;
        internal DbSet<T> _objectSet;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the context.
        /// </summary>
        /// <value>
        /// The context.
        /// </value>
        public virtual DbContext Context
        {
            get { return _context; }
            set { _context = value; }
        }
        

        /// <summary>
        /// Gets or sets the save messages.
        /// </summary>
        /// <value>
        /// The save messages.
        /// </value>
        public virtual List<string> ErrorMessages { get; set; }

        /// <summary>
        /// Gets a LINQ expression parameter.
        /// </summary>
        /// <value>
        /// The parameter expression.
        /// </value>
        public ParameterExpression ParameterExpression { get; } = Expression.Parameter( typeof( T ), "p" );

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Service{T}"/> class.
        /// </summary>
        /// <param name="dbContext">The db context.</param>
        public Service( DbContext dbContext )
        {
            _context = dbContext;
            _objectSet = _context.Set<T>();
        }

        #endregion

        #region Methods

        #region Queryable
        
        /// <summary>
        /// Gets an <see cref="IQueryable{T}"/> list of all models
        /// </summary>
        /// <returns></returns>
        public virtual IQueryable<T> Queryable()
        {
            return _objectSet;
        }

        /// <summary>
        /// Gets an <see cref="IQueryable{T}"/> list of all models
        /// with eager loading of properties specified in includes
        /// </summary>
        /// <returns></returns>
        public virtual IQueryable<T> Queryable( string includes )
        {
            DbQuery<T> value = _objectSet;
            if ( !String.IsNullOrEmpty( includes ) )
            {
                foreach ( var include in includes.SplitDelimitedValues() )
                {
                    value = value.Include( include );
                }
            }
            return value;
        }

        #endregion

        #region Get Methods

        /// <summary>
        /// Gets the model with the id value
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="isTracked"></param>
        /// <returns></returns>
        public virtual T Get( int id, bool isTracked = true )
        {
            if ( isTracked )
            {
                return Queryable().FirstOrDefault( t => t.Id == id ); 
            }
            return Queryable().AsNoTracking().FirstOrDefault( t => t.Id == id );
        }
        
        /// <summary>
        /// Gets all entities.
        /// </summary>
        /// <returns></returns>
        public virtual IQueryable<T> GetAll( )
        {
            return _objectSet.AsQueryable();
        }


        /// <summary>
        /// Trys to get the model with the id value
        /// </summary>
        /// <returns></returns>
        public virtual bool TryGet( int id, out T item )
        {
            item = Get( id );
            if ( item == null )
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Gets entities from a list of ids.
        /// </summary>
        /// <param name="ids">The ids.</param>
        /// <returns></returns>
        public virtual IQueryable<T> GetByIds( List<int> ids )
        {
            return Queryable().Where( t => ids.Contains( t.Id ) );
        }

       
        /// <summary>
        /// Gets a list of entities by ids.
        /// </summary>
        /// <param name="ids">The ids.</param>
        /// <returns></returns>
        public virtual List<T> GetListByIds( List<int> ids )
        {
            return GetByIds( ids ).ToList();
        }



        #endregion

        #region Attach

        /// <summary>
        /// Attaches the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        public virtual void Attach( T item )
        {
            _objectSet.Attach( item );
        }

        public virtual void Update( T item )
        {
            _objectSet.AddOrUpdate( i => i.Id, item );
        }

        #endregion

        #region Add

        /// <summary>
        /// Adds the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        public virtual void Add( T item )
        {
            _objectSet.Add( item );
        }


        /// <summary>
        /// Adds the range.
        /// </summary>
        /// <param name="items">The items.</param>
        /// <returns></returns>
        public virtual bool AddRange( IEnumerable<T> items )
        {
            _objectSet.AddRange( items );
            return true;
        }

        #endregion

        #region Delete


        /// <summary>
        /// Deletes the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        public virtual bool Delete( T item )
        {
            _objectSet.Remove( item );
            return true;
        }


        /// <summary>
        /// Deletes the range.
        /// </summary>
        /// <param name="items">The items.</param>
        /// <returns></returns>
        public virtual bool DeleteRange( IEnumerable<T> items )
        {
            _objectSet.RemoveRange( items );
            return true;
        }

        #endregion

        #region Other

        /// <summary>
        /// Creates a raw sql query that will return entities
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public IEnumerable<T> ExecuteQuery( string query, params object[] parameters )
        {
            return _objectSet.SqlQuery( query, parameters );
        }

        /// <summary>
        /// Anys the specified parameter expression.
        /// </summary>
        /// <param name="parameterExpression">The parameter expression.</param>
        /// <param name="whereExpression">The where expression.</param>
        /// <returns></returns>
        public bool Any( ParameterExpression parameterExpression, Expression whereExpression )
        {
            var lambda = Expression.Lambda<Func<T, bool>>( whereExpression, parameterExpression );
            return this.Queryable().Any( lambda );
        }




        #endregion

        #endregion
    }

}