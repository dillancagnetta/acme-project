using Acme.Data;
using Acme.Domain.Model;

namespace Acme.Migrations.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<AcmeContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(AcmeContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
                context.Employee.AddOrUpdate(
                  e => e.EmployeeNum,
                  new Employee
                  {
                      Person = new Person { FirstName = "Andrew", LastName = "Peters", BirthDate = DateTime.Today },
                      EmployedDate = DateTime.Today,
                      EmployeeNum = "123456789"
                  },
                  new Employee
                  {
                      Person = new Person { FirstName = "Brice", LastName = "Lambson", BirthDate = DateTime.Today },
                      EmployedDate = DateTime.Today,
                      EmployeeNum = "987654321"
                  }
                );
            
        }
    }
}
