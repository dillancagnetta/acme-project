﻿namespace Acme.Domain
{
    /// <summary>
    /// Interface for all code-first entities
    /// </summary>
    public interface IEntity
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>
        /// The id.
        /// </value>
        int Id { get; set; }

       
    }
}
