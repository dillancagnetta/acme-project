﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Acme.Domain.Model
{
    [DataContract]
    public class Person : Model<Person>
    {
        #region Entity Properties

        [DataMember]
        [MaxLength(128)]
        public string FirstName { get; set; }

        [DataMember]
        [MaxLength( 128 )]
        public string LastName { get; set; }

        [DataMember]
        [DataType( DataType.DateTime )]
        public DateTime BirthDate { get; set; }

        #endregion
    }
}
