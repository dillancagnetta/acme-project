﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace Acme.Domain.Model
{
    [DataContract]
    public class Employee : Model<Employee>
    {
        #region Entity Properties

        [DataMember]
        [MaxLength( 16 )]
        public string EmployeeNum { get; set; }

        [DataMember]
        [DataType( DataType.DateTime )]
        public DateTime EmployedDate { get; set; }

        [DataMember]
        [DataType(DataType.DateTime)]
        public DateTime? TerminatedDate { get; set; }

        public int PersonId { get; set; }

        #endregion

        #region Navigation Properties

        public virtual Person Person { get; set; }

        #endregion
    }
}
