﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Acme.Domain
{
    /// <summary>
    /// Represents an entity object and is the base class for all model objects to inherit from
    /// </summary>
    /// <typeparam name="T">The Type entity that is being referenced <example>Entity&lt;Person&gt;</example></typeparam>
    [DataContract]
    public abstract class Entity<T> : IEntity where T : Entity<T>, new()
    {
        #region Entity Properties
        
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>
        /// The id.
        /// </value>
        [Key]        
        [DataMember]
        public int Id { get; set; }

       
        #endregion
        
    }

 

}