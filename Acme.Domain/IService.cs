﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Acme.Domain
{
    public interface IService<T>
    {
        /// <summary>
        /// Gets or sets the context.
        /// </summary>
        /// <value>
        /// The context.
        /// </value>
        DbContext Context { get; set; }

        /// <summary>
        /// Gets an <see cref="IQueryable{T}"/> list of all models
        /// </summary>
        /// <returns></returns>
        IQueryable<T> Queryable();

        /// <summary>
        /// Gets the model with the id value
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="isTracked"></param>
        /// <returns></returns>
        T Get( int id, bool isTracked = true );

        /// <summary>
        /// Gets all entities.
        /// </summary>
        /// <returns></returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// Trys to get the model with the id value
        /// </summary>
        /// <returns></returns>
        bool TryGet( int id, out T item );

        /// <summary>
        /// Gets entities from a list of ids.
        /// </summary>
        /// <param name="ids">The ids.</param>
        /// <returns></returns>
        IQueryable<T> GetByIds( List<int> ids );

        /// <summary>
        /// Gets a list of entities by ids.
        /// </summary>
        /// <param name="ids">The ids.</param>
        /// <returns></returns>
        List<T> GetListByIds( List<int> ids );

        /// <summary>
        /// Attaches the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        void Attach( T item );

        /// <summary>
        /// Updates the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        void Update( T item );

        /// <summary>
        /// Adds the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        void Add( T item );

        /// <summary>
        /// Adds the range.
        /// </summary>
        /// <param name="items">The items.</param>
        /// <returns></returns>
        bool AddRange( IEnumerable<T> items );

        /// <summary>
        /// Deletes the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        bool Delete( T item );

        /// <summary>
        /// Deletes the range.
        /// </summary>
        /// <param name="items">The items.</param>
        /// <returns></returns>
        bool DeleteRange( IEnumerable<T> items );

        /// <summary>
        /// Creates a raw sql query that will return entities
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        IEnumerable<T> ExecuteQuery( string query, params object[] parameters );

        /// <summary>
        /// Anys the specified parameter expression.
        /// </summary>
        /// <param name="parameterExpression">The parameter expression.</param>
        /// <param name="whereExpression">The where expression.</param>
        /// <returns></returns>
        bool Any( ParameterExpression parameterExpression, Expression whereExpression );
    }
}
