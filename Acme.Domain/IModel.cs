﻿using System;

namespace Acme.Domain
{
    /// <summary>
    /// Interface for all models
    /// </summary>
    public interface IModel : IEntity
    {
        /// <summary>
        /// Gets or sets the created date time.
        /// </summary>
        /// <value>
        /// The created date time.
        /// </value>
        DateTime? CreatedDateTime { get; set; }

        /// <summary>
        /// Gets or sets the modified date time.
        /// </summary>
        /// <value>
        /// The modified date time.
        /// </value>
        DateTime? ModifiedDateTime { get; set; }
                

    }
}
