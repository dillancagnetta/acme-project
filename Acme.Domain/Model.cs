﻿using System;
using System.Runtime.Serialization;

namespace Acme.Domain
{
    /// <summary>
    /// Represents an entity
    /// </summary>
    [DataContract]
    public abstract class Model<T> : Entity<T>, IModel where T : Model<T>, new()
    {
        #region Entity Properties

        /// <summary>
        /// Gets or sets the created date time.
        /// </summary>
        /// <value>
        /// The created date time.
        /// </value>
        [DataMember]
        public DateTime? CreatedDateTime { get; set; }

        /// <summary>
        /// Gets or sets the modified date time.
        /// </summary>
        /// <value>
        /// The modified date time.
        /// </value>
        [DataMember]
        public DateTime? ModifiedDateTime { get; set; }

        #endregion Entity Properties
    }
}