﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Formatting = System.Xml.Formatting;

namespace Acme.Core.ExtensionMethods
{
    /// <summary>
    /// JSON Extensions
    /// </summary>
    public static partial class ExtensionMethods
    {
        #region Json Extensions

        /// <summary>
        /// Converts object to JSON string
        /// </summary>
        /// <param name="obj">Object.</param>
        /// <returns></returns>
        public static string ToJson( this object obj )
        {
            return JsonConvert.SerializeObject( obj);
        }

        /// <summary>
        /// Attempts to deserialize a json string into T.  If it can't be deserialized, returns null
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="val">The value.</param>
        /// <returns></returns>
        public static T FromJsonOrNull<T>( this string val )
        {
            try
            {
                if ( string.IsNullOrWhiteSpace( val ) )
                {
                    return default( T );
                }
                else
                {
                    return JsonConvert.DeserializeObject<T>( val );
                }
            }
            catch
            {
                return default( T );
            }
        }

        #endregion

        #region JObject extension methods

        /// <summary>
        /// Converts a jObject to a dictionary
        /// </summary>
        /// <param name="jobject">The jobject.</param>
        /// <returns></returns>
        public static IDictionary<string, object> ToDictionary( this JObject jobject )
        {
            var result = jobject.ToObject<Dictionary<string, object>>();

            var valueKeys = result
                .Where( r => r.Value != null && r.Value.GetType() == typeof( JObject ) )
                .Select( r => r.Key )
                .ToList();

            var arrayKeys = result
                .Where( r => r.Value != null && r.Value.GetType() == typeof( JArray ) )
                .Select( r => r.Key )
                .ToList();

            arrayKeys.ForEach( k => result[k] = ( (JArray)result[k] ).Values().Select( v => ( (JValue)v ).Value ).ToArray() );
            valueKeys.ForEach( k => result[k] = ToDictionary( result[k] as JObject ) );

            return result;
        }

        #endregion
    }
}
