﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Acme.Core.ExtensionMethods
{
    public static class HttpClientExtensions
    {
        public static async Task<HttpResponseMessage> PatchAsync( this HttpClient client, string address, HttpContent content )
        {
            var method = new HttpMethod( "PATCH" );
            var request = new HttpRequestMessage( method, address )
            {
                Content = content,                
            };

            var response = new HttpResponseMessage();

            try
            {
                response = await client.SendAsync( request );
            }
            catch ( Exception e )
            {
                response.StatusCode = HttpStatusCode.InternalServerError;
                return response;
            }

            return response;
        }

        public static async Task<HttpResponseMessage> PutAsync( this HttpClient client, string address, HttpContent content )
        {
            var method = new HttpMethod( "PUT" );
            var request = new HttpRequestMessage( method, address )
            {
                Content = content,
            };

            var response = new HttpResponseMessage();

            try
            {
                response = await client.SendAsync( request );
            }
            catch ( Exception e )
            {
                response.StatusCode = HttpStatusCode.InternalServerError;
                return response;
            }

            return response;
        }
    }
}