﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Acme.Domain.Model;

namespace Acme.Data
{
    /// <summary>
    /// Entity Framework Context
    /// </summary>
    public class AcmeContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AcmeContext"/> class.
        /// </summary>
        [System.Runtime.CompilerServices.MethodImpl( System.Runtime.CompilerServices.MethodImplOptions.NoInlining )]
        public AcmeContext() : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AcmeContext"/> class.
        /// Use this if you need to specify a connection string other than the default
        /// </summary>
        /// <param name="nameOrConnectionString">Either the database name or a connection string.</param>
        public AcmeContext( string nameOrConnectionString ) : base( nameOrConnectionString )
        {
        }

        #region Models

        public DbSet<Person> Person { get; set; }
        public DbSet<Employee> Employee { get; set; }

        #endregion Models

        /// <summary>
        /// This method is called when the context has been initialized, but
        /// before the model has been locked down and used to initialize the context.
        /// </summary>
        /// <param name="modelBuilder">The builder that defines the model for the context being created.</param>
        protected override void OnModelCreating( DbModelBuilder modelBuilder )
        {
            //base.OnModelCreating( modelBuilder );
            ContextHelper.AddConfigurations( modelBuilder );
        }
    }

    /// <summary>
    ///
    /// </summary>
    public static class ContextHelper
    {
        /// <summary>
        /// Adds the configurations.
        /// </summary>
        /// <param name="modelBuilder">The model builder.</param>
        public static void AddConfigurations( DbModelBuilder modelBuilder )
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Configurations.AddFromAssembly( typeof( AcmeContext ).Assembly );
        }
    }
}