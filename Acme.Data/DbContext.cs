﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using Acme.Domain;

namespace Acme.Data
{
    /// <summary>
    /// Entity Framework Context
    /// </summary>
    public abstract class DbContext : System.Data.Entity.DbContext
    {
        private bool _transactionInProgress;

        /// <summary>
        /// Initializes a new instance of the <see cref="DbContext"/> class.
        /// </summary>
        protected DbContext() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DbContext"/> class.
        /// </summary>
        /// <param name="nameOrConnectionString">Either the database name or a connection string.</param>
        protected DbContext( string nameOrConnectionString ) : base( nameOrConnectionString ) { }

        /// <summary>
        /// Gets any error messages that occurred during a SaveChanges
        /// </summary>
        /// <value>
        /// The save error messages.
        /// </value>
        public virtual List<string> SaveErrorMessages { get; private set; }

        /// <summary>
        /// Wraps code in a BeginTransaction and CommitTransaction
        /// </summary>
        /// <param name="action">The action.</param>
        public void WrapTransaction( Action action )
        {
            if ( !_transactionInProgress )
            {
                _transactionInProgress = true;
                using ( var dbContextTransaction = this.Database.BeginTransaction() )
                {
                    try
                    {
                        action.Invoke();
                        dbContextTransaction.Commit();
                    }
                    catch
                    {
                        dbContextTransaction.Rollback();
                        throw;
                    }
                    finally
                    {
                        _transactionInProgress = false;
                    }
                }
            }
            else
            {
                action.Invoke();
            }
        }

        /// <summary>
        /// Saves all changes made in this context to the underlying database.
        /// </summary>
        /// <returns>
        /// The number of objects written to the underlying database.
        /// </returns>
        public override int SaveChanges()
        {
            return SaveChanges( false );
        }

        public int SaveChanges( bool disablePrePostProcessing )
        {
            // Pre and Post processing has been disabled, just call the base
            // SaveChanges() method and return
            if ( disablePrePostProcessing )
            {
                return base.SaveChanges();
            }

            int result = 0;

            SaveErrorMessages = new List<string>();

            // Evaluate the current context for items that have changes
            var updatedItems = PreSave( this );
            
            if ( updatedItems != null )
            {
                try
                {
                    // Save the context changes
                    result = base.SaveChanges();
                }
                catch ( System.Data.Entity.Validation.DbEntityValidationException ex )
                {
                    var validationErrors = new List<string>();
                    foreach ( var error in ex.EntityValidationErrors )
                    {
                        foreach ( var prop in error.ValidationErrors )
                        {
                            validationErrors.Add( $"{error.Entry.Entity.GetType().Name} ({prop.PropertyName}): {prop.ErrorMessage}" );
                        }
                    }

                    throw new SystemException( "Entity Validation Error: " + validationErrors, ex );
                }                
            }

            return result;
        }

        protected virtual List<ContextItem> PreSave( DbContext dbContext )
        {
            // Then loop again, as new models may have been added by PreSaveChanges events
            var updatedItems = new List<ContextItem>();
            foreach ( var entry in dbContext.ChangeTracker.Entries()
                .Where( c =>
                    c.Entity is IEntity &&
                    ( c.State == EntityState.Added || c.State == EntityState.Modified || c.State == EntityState.Deleted ) ) )
            {
                // Cast entry as IEntity
                var entity = entry.Entity as IEntity;

                // Get the context item to track audits
                var contextItem = new ContextItem( entity, entry );

                // If entity was added or modified, update the Created/Modified fields
                if ( entry.State == EntityState.Added || entry.State == EntityState.Modified )
                {
                    if ( entry.Entity is IModel )
                    {
                        var model = entry.Entity as IModel;

                        // Update Created/Modified times
                        if ( entry.State == EntityState.Added )
                        {
                            if ( !model.CreatedDateTime.HasValue )
                            {
                                model.CreatedDateTime = DateTime.Now;
                            }
                           
                            model.ModifiedDateTime = DateTime.Now;
                            
                        }
                        else if ( entry.State == EntityState.Modified )
                        {
                            model.ModifiedDateTime = DateTime.Now;
                            
                        }
                    }
                }

                updatedItems.Add( contextItem );
            }

            return updatedItems;
        }

        /// <summary>
        /// State of entity being changed during a context save
        /// </summary>
        protected class ContextItem
        {
            /// <summary>
            /// Gets or sets the entity.
            /// </summary>
            /// <value>
            /// The entity.
            /// </value>
            public IEntity Entity { get; set; }

            /// <summary>
            /// Gets or sets the state.
            /// </summary>
            /// <value>
            /// The state.
            /// </value>
            public EntityState State => this.DbEntityEntry.State;

            /// <summary>
            /// Gets or sets the database entity entry.
            /// </summary>
            /// <value>
            /// The database entity entry.
            /// </value>
            public DbEntityEntry DbEntityEntry { get; set; }

            /// <summary>
            /// Initializes a new instance of the <see cref="ContextItem" /> class.
            /// </summary>
            /// <param name="entity">The entity.</param>
            /// <param name="dbEntityEntry">The database entity entry.</param>
            public ContextItem( IEntity entity, DbEntityEntry dbEntityEntry )
            {
                Entity = entity;
                DbEntityEntry = dbEntityEntry;                

                switch ( dbEntityEntry.State )
                {
                    case EntityState.Added:
                        {                            
                            break;
                        }
                    case EntityState.Deleted:
                        {                            
                            break;
                        }
                    case EntityState.Modified:
                        {                            
                            break;
                        }
                }
            }
        }

    }
}
