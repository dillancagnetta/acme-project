﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Acme.Data;
using Acme.Domain;
using Acme.Domain.Model;
using Acme.Services.Model;
using Newtonsoft.Json.Serialization;

namespace Acme.Api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );


            // format Json
            config.Formatters.JsonFormatter.SerializerSettings.Formatting
                    = Newtonsoft.Json.Formatting.Indented;

            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver
                    = new CamelCasePropertyNamesContractResolver();

            // remove XML formatter
            config.Formatters.XmlFormatter.SupportedMediaTypes.Clear();
        }
    }
}
