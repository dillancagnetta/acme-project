﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Acme.Domain;
using Acme.Services;

namespace Acme.Api
{
    public abstract class ApiController<T> : ApiController where T : Entity<T>, new()
    {
        /// <summary>
        /// Gets or sets the service.
        /// </summary>
        /// <value>
        /// The service.
        /// </value>
        protected IService<T> Service { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ApiController{T}"/> class.
        /// </summary>
        /// <param name="service">The service.</param>
        protected ApiController( IService<T> service )
        {
            Service = service;

            // Turn off proxy creation by default so that when querying objects through rest, EF does not automatically navigate all child properties for requested objects
            // When adding, updating, or deleting objects, the proxy should be enabled to properly track relationships that should or shouldn't be updated
            //SetProxyCreation( false );
        }

        #region Methods

        // GET api/<controller>
        public virtual IQueryable<T> Get(  )
        {
            var result = Service.Queryable( );
            return result;
        }

        // GET api/<controller>/5
        public virtual T Get( int id )
        {
            T model;
            if ( !Service.TryGet( id, out model ) )
            {
                throw new HttpResponseException( HttpStatusCode.NotFound );
            }

            return model;
        }

        // POST api/<controller> (insert)
        public virtual HttpResponseMessage Post( [FromBody] T value )
        {
            if ( value == null )
            {
                throw new HttpResponseException( HttpStatusCode.BadRequest );
            }

            SetProxyCreation( true );

            Service.Add( value );

            Service.Context.SaveChanges();

            var response = ControllerContext.Request.CreateResponse( HttpStatusCode.Created, value.Id );

            return response;
        }

        // DELETE api/<controller>/5
        public virtual void Delete( int id )
        {
            SetProxyCreation( true );

            T model;
            if ( !Service.TryGet( id, out model ) )
            {
                throw new HttpResponseException( HttpStatusCode.NotFound );
            }

            Service.Delete( model );
            Service.Context.SaveChanges();
        }

        #endregion

        /// <summary>
        /// Gets or sets a value indicating whether [enable proxy creation].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [enable proxy creation]; otherwise, <c>false</c>.
        /// </value>
        protected void SetProxyCreation( bool enabled )
        {
            Service.Context.Configuration.ProxyCreationEnabled = enabled;
        }
    }
}