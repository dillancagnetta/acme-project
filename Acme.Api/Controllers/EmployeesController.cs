﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Acme.Data;
using Acme.Domain.Model;
using Acme.Services;
using Acme.Services.Dto;
using Acme.Services.Interfaces;
using Acme.Services.Model;

namespace Acme.Api.Controllers
{
    public class EmployeesController : ApiController
    {
        private readonly IEmployeeService _service;

        public EmployeesController( IEmployeeService service )
        {
            _service = service;
        }

        public EmployeesController() : this( new EmployeeService( new AcmeContext() ) ) { }

        // GET: api/Employees
        public IEnumerable<EmployeeDto> Get()
        {
            return  _service.GetEmployees();
        }

        // GET: api/Employees/5
        public IHttpActionResult Get( int id )
        {
            var employee = _service.GetEmployee( id );

            if ( employee == null ) return NotFound();

            return Ok( employee );
        }

        // POST api/<controller> (insert)
        public HttpResponseMessage Post( [FromBody] EmployeeDto dto )
        {
            if ( dto == null )
            {
                throw new HttpResponseException( HttpStatusCode.BadRequest );
            }

            var model = _service.FromDto( dto );

            _service.Add( model );

            _service.Context.SaveChanges();

            var response = ControllerContext.Request.CreateResponse( HttpStatusCode.Created, model.Id );

            return response;
        }

    }



}
