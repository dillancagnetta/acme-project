﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Acme.Data;
using Acme.Domain.Model;
using Acme.Services;
using Acme.Services.Model;

namespace Acme.Api.Controllers
{
    public class PersonController : ApiController<Person>
    {
        public PersonController(  ) : base( new PersonService( new AcmeContext() ) )  { }
    }
}
